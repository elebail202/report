    ## NOTES SUR LE STAGE 

### Tutor : Victor Casula

### Department : Research unit of Medical Imaging, Physics and Technology


<ins>*Mon 30/05*</ins> : Meeting of introduction on the subject and on the main goal of the research team (develop a tool that can analyses MRI images, knee cartilage in particular)

<ins>*Tue 31/05*</ins> : on the morning, documentation to read during all the week. Meeting on a second more detailed presentation of the actual situation of the team. Tasks have been distributed.

<ins>*Wed 01/06*</ins> : work in autonomy on the material sent by my tutor.

<ins>*Thu 02/06*</ins> : meeting with Heta in order to get some information for students at the university (student card and fees). Work in autonomy on the material given by Victor.

<ins>*Fri 03/06*</ins> : work in autonomy, finish reading the material. Victor sent functions of Mokkula to study next week.

<ins>*Mon 06/06*</ins> : work in autonomy on the functions Victor sent me, lecture of the different codes

<ins>*Tue 07/06*</ins> : Meeting with Matti Juntti, one person of the team who gave me tips to understanding the codes. Then, I applied his tips. I made the codes clearer and easier to read. (improve the lisibility).

<ins>*Wed 08/06*</ins> : Work on the functions on matlab : finishing improving the lisibility + try to understand the first functions.

<ins>*Thu 09/06*</ins> : understanding of the different functions. Lecture of the 2 articles sended by Victor on texture analysis (written by the research unit)

<ins>*Fri 10/06*</ins> : trying to run the functions with some data, asking help to Matti who read the codes that I formatted. So I can have a feedback on what I did during the week.

<ins>*Mon 13/06*</ins> : reading of a paper on requirements engineering and some changes on the functions

<ins>*Tue 14/06*</ins> : Little ameliorations on the functions to improve the refactoring, using tool diff on linux. Learning about Markdown language. Page in markdown to train.

<ins>*Wed 15/06*</ins> : Installing Git and getting familiar to it.

<ins>*Thu 16/06*</ins> : Meeting with Matti to have clearer ideas on how using Git (branches, repos…)

<ins>*Fri 17/06*</ins> : Meeting with Victor to show him what I did and to take the data which would be useful to use Mokkula and to test the functions. To be continued on Monday morning because he had to go. I also wrote a file on key-terms Matti gave me to have clearer ideas.

<ins>*Mon 20/06*</ins> : Meeting with Victor, we defined my different tasks for the internship and figured out how I could launch and use Mokkula on my computer. He gave some data I could use (anonymous data). Trying to have an older version of Matlab on the university computer (MatlabR2018b instead of R2020b). Trying to test the functions with the data I finally have and trying to understand what these textures functions are really doing.

<ins>*Tue 21/06*</ins> : Meeting with Sanam Assili who showed us how to make segmentation on T2 images by using Aedes and Mokkula. Meeting with Matti on the next tasks (Requirements). Import all my folders in gitlab to share it. Access to the website repo Git.

<ins>*Wed 22/06*</ins> : Trying to fix an error on the convert_segmentation_to_dicom_final function. This one was working once on Monday and is not working anymore...Maybe a folder problem, or a path problem...
Trying to start a test server on my computer for the website.

<ins>*Thu 23/06*</ins> : Error on the first function solved and I began to study the second question but some files are missing some I am trying to figure out to what they refers to. I had a workshop cartilage analysis on the afternoon where people presented datas and the history of Mokkula and its creation.

<ins>*Fri 24/06*</ins> : Weekend day

<ins>*Mon 27/06*</ins> : Trying to launch the test server for the website. Work on the second texture function.  
Installing Ruby and Jekyll for the website. I can now have a local access to the server. 

<ins>*Tue 28/06*</ins> : Work on the second texture function. Bbq research group

<ins>*Wed 29/06*</ins> : Work on the second texture function. Meeting with Matti to discuss the workshop, and the next tasks to do. Talked a little about UML.

<ins>*Thu 30/06*</ins> : Work on the second texture function, lot of errors occured in many different functions.

<ins>*Fri 01/07*</ins> : Read and study about UML. Work on refactoring additional functions.

<ins>*Mon 04/07*</ins> : Advances on the second function to fix all the errors and bug of compilation. Beginning of the redaction of the internship report in LaTex (mainly the first page for now).

<ins>*Tue 05/07*</ins> : Trying to fix all the errors with the second function...quite long...Continuing the redaction of my report. Starting to write a software requirement on the website.

<ins>*Wed 06/07*</ins> : Writing all the software requirements I have for now on the website.

<ins>*Thu 07/07*</ins> : Work on the software requirements. Work on the texture functions (the second one still)

<ins>*Fri 08/07*</ins> : Work on the sofware requirements.

<ins>*Mon 11/07*</ins> : Work on the texture functions

<ins>*Tue 12/07*</ins> : Work on the form of my internship report on LaTex. Work on the texture functions on Matlab.

<ins>*Wed 13/07*</ins> : Work on the textures functions and on my documentation file. Work on the website to change the font used. We want a _sans-serif_ font. 

<ins>*Thu 14/07*</ins> : Work on the textures functions

<ins>*Fri 15/07*</ins> : Work on the textures functions

<ins>*Mon 18/07*</ins> : Work on the textures functions. Trying to understand what is wrong with roilist and Timage.

<ins>*Tue 19/07*</ins> : Meeting with Matti, discussion about my work, the documentation and the next tasks to do.

<ins>*Wed 20/07*</ins> : Work on the form of the data dictionary. Work on the texture functions (well advanced).

<ins>*Thu 21/07*</ins> : Understand what a .json file is. Work on the texture functions (almost finished) !! Begin the data dictionnary on a .json file.

<ins>*Fri 22/07*</ins> : Work on the data dictionary

<ins>*Mon 25/07*</ins> : Work on the data dictionary of the first function and designed a function dependencies diagram.

<ins>*Tue 26/07*</ins> : Work on the data dictionary of the second function. Add details in the function documentation.

<ins>*Wed 27/07*</ins> : Work on the plugin files modified at to get automatically the modification time of the requirements files. This will be insert in the website.

<ins>*Thu 28/07*</ins> : Finished the plugin to get automatically the modification time of the requirements files. Trying to fix the pb of roipoly in the runflat function.

<ins>*Fri 29/07*</ins> : Work on the roipoly of the textures functions. Trying to find a great typeface for the website. First rendering.

<ins>*Mon 01/08*</ins> : Work on the website to do something more eye catching.

<ins>*Tue 02/08*</ins> : Work on the website to do something more eye catching. Changing typeface family, colors, table...

<ins>*Wed 03/08*</ins> : Working on the color of the links in the markdown files. Finished a first version of the website. Work on the texture function bc not the right image.

<ins>*Thu 04/08*</ins> : Work on the textures functions. Work on the plan of my report.

<ins>*Fri 05/08*</ins> : Meeting with Matti on the advance on the project and what to modify.

<ins>*Mon 08/08*</ins> : Changes made on what we talked about with Matti. Begin to work on the footer of the website that I will entirely design. Reading of articles to inform me on what to do.

<ins>*Tue 09/08*</ins> : Work on the footer of the website.

<ins>*Wed 10/08*</ins> : Finished the footer of the website. Work on the bibli of my report.

<ins>*Thu 11/08*</ins> : Meeting with Matti to see what to change and what to do next.

<ins>*Fri 12/08*</ins> : Work on the footer to make the modifications we discussed at the last meeting. Improvements on affordance.

<ins>*Mon 15/08*</ins> : Work on the documentation of the textures functions.

<ins>*Tue 16/08*</ins> : Work on the texture functions. Refactoring of format_results.m

<ins>*Wed 17/08*</ins> : Work on the documentation file. Meeting with Victor to do a recap of the internship.

<ins>*Thu 18/08*</ins> : Work on the final file that I will let to the team.

<ins>*Fri 19/08*</ins> : Last meeting with Matti and work on the final folder that I will let to the team.
